package controllers;

import models.User;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.usersList;
import views.html.userAcount;
import views.html.loginPage;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sasha on 10.03.2016.
 */
public class App extends Controller {
    public Result index (){
        return ok(index.render("cococo"));
    }

    @Transactional
    public Result addUser (){
        User user = Form.form(User.class).bindFromRequest().get();
        JPA.em().persist(user);
        return ok(loginPage.render("Login please"));
    }

    @Transactional
    public Result getUsers (){
        Query q = JPA.em().createQuery("select e from User e");
        List<User> users = q.getResultList();
        return ok(usersList.render(users));
    }

    @Transactional
    public Result login (){

        Query q = JPA.em()
                .createQuery("select user from User as user where user.email = ?1")
                .setParameter(1, Form.form().bindFromRequest().get("email"));
        User user = (User)q.getSingleResult();
        //TODO null pointer
       if(Form.form().bindFromRequest().get("password").equals(user.getPassword())){
            return ok(userAcount.render(user));}
        else return ok(loginPage.render("wrong password"));

    }

    @Transactional
    public User getUserById (int id){
        //todo: null pointer ex
        User user = JPA.em().find(User.class, id);
        return user;
    }

    @Transactional
    public void removeUser (int id){
        //todo: null pointer ex
        User user = getUserById(id);
        JPA.em().remove(user);
    }

}
