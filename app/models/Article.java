package models;

import javax.persistence.*;

@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String header;

    @Column
    private String text;

    public Article() {
    }

    public Article(Integer id, String header, String text) {
        this.id = id;
        this.header = header;
        this.text = text;
    }

    @Override
    public String toString() {
        return "***Article***" + System.lineSeparator()
                + header + System.lineSeparator()
                + text + System.lineSeparator() + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
