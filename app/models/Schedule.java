package models;

import java.util.ArrayList;
import java.util.List;

public class Schedule {

	private List<ScheduleItem> items = new ArrayList<ScheduleItem>();

	public ScheduleItem[] getShows() {
		return (ScheduleItem[]) items.toArray();
	}

	public void addItem(ScheduleItem item) {
		items.add(item);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!items.isEmpty()){
			sb.append("##########SCHEDULE##########").append(System.lineSeparator());
			for (ScheduleItem scheduleItem : items) {
				sb.append(scheduleItem).append(System.lineSeparator());
			}
			sb.append(System.lineSeparator());
		}
		return sb.toString();
	}
}