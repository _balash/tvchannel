package models;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

import models.mediacontent.AuthorShow;

import javax.persistence.*;

@Entity
@Table(name = "presenter")
public class Presenter {

	@Id
	@GeneratedValue
	private Integer id;

	@Column
	private String name;

	@Column
	private LocalDate dob;

	@Column
	private String description;

	@ManyToMany (fetch = FetchType.LAZY)
	private List<AuthorShow> shows;

	public Presenter() {
		this.shows = new ArrayList<AuthorShow>();
	}

	public Presenter(Integer id, String name, LocalDate dob, String description) {
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.description = description;
		this.shows = new ArrayList<AuthorShow>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("***Presenter***").append(System.lineSeparator())
				.append("name : ").append(name )
				.append(", date of birth : ").append(dob.toString("dd.MM.yyyy"))
				.append(System.lineSeparator()).append(description).append(System.lineSeparator());
		if (!shows.isEmpty()) {
			for (AuthorShow show : shows) {
				sb.append(show.getName()).append(System.lineSeparator());
			}
		}
		sb.append(System.lineSeparator());
		return sb.toString();
	}

	public void addShow(AuthorShow show) {
		shows.add(show);
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getDob() {
		return dob;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AuthorShow[] getShows() {
		return (AuthorShow[]) shows.toArray();
	}

}
