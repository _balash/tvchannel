package models;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import models.mediacontent.Show;

import javax.persistence.*;

@Entity
@Table(name = "schedule_item")
public class ScheduleItem {

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private LocalDate date;

    @Column
    private LocalTime startTime;

    @Column
    private LocalTime endTime;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "show_id")
    private Show show;

    public ScheduleItem() {
    }

    public ScheduleItem(Integer id, LocalDate date, LocalTime startTime, LocalTime endTime, Show show) {
        super();
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.show = show;
    }

    @Override
    public String toString() {
        return "***ScheduleItem***" + System.lineSeparator()
                + "date : " + date.toString("dd.MM.yyyy") + System.lineSeparator()
                + show.getName() + System.lineSeparator()
                + "time : " + startTime.toString("HH:mm:ss") + " - " + endTime.toString("HH:mm:ss") + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }
}