package models.mediacontent;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name="episodic_show")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@AttributeOverrides({
		@AttributeOverride (name = "id", column = @Column(name="id")),
		@AttributeOverride (name = "name", column = @Column(name="name")),
		@AttributeOverride (name = "genres", column = @Column(name="genres")),
		@AttributeOverride (name = "description", column = @Column(name="description")),
		@AttributeOverride (name = "url", column = @Column(name="url")),
})
public abstract class EpisodicShow extends Show {
//  @OneToMany(mappedBy = "episodic_show")
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name="episodes")
	private List<Episode> episodes;

	public EpisodicShow() {
		this.episodes = new ArrayList<Episode>();
	}

	public EpisodicShow(Integer id, String name, String description) {
		super(id, name, description);
		this.episodes = new ArrayList<Episode>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		if (!episodes.isEmpty()){
			sb.append("Episodes:").append(System.lineSeparator());
			for (Episode episode : episodes) {
				sb.append("\t").append(episode);
			}
		}
		return sb.toString();
	}

	public void addEpisode(Episode episode) {
		episodes.add(episode);
	}

	public Episode[] getEpisodes() {
		return (Episode[]) episodes.toArray();
	}
}
