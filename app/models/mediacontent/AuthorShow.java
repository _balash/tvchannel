package models.mediacontent;

import java.util.ArrayList;
import java.util.List;

import models.Presenter;

import javax.persistence.*;

@Entity
@Table(name="author_show")
@AttributeOverrides({
		@AttributeOverride (name = "id", column = @Column(name="id")),
		@AttributeOverride (name = "name", column = @Column(name="name")),
		@AttributeOverride (name = "genres", column = @Column(name="genres")),
		@AttributeOverride (name = "description", column = @Column(name="description")),
		@AttributeOverride (name = "url", column = @Column(name="url")),
		@AttributeOverride (name = "episodes", column = @Column(name="episodes"))
})
public class AuthorShow extends EpisodicShow {

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "author_show_presenter", joinColumns = {
			@JoinColumn(name = "show_id", nullable = false, updatable = false)},
			inverseJoinColumns = { @JoinColumn(name = "presenter_id",
					nullable = false, updatable = false)})
	private List<Presenter> presenters;

	public AuthorShow() {
		super();
		this.presenters = new ArrayList<Presenter>();
	}

	public AuthorShow(Integer id, String name, String description) {
		super(id, name, description);
		this.presenters = new ArrayList<Presenter>();
	}
	
	public void addPresenter(Presenter presenter){
		presenters.add(presenter);
	}

	public Presenter[] getPresenters() {
		return (Presenter[]) presenters.toArray();
	}

}
