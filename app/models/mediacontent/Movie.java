package models.mediacontent;

import javax.persistence.*;

@Entity
@Table (name="movie")
@AttributeOverrides({
		@AttributeOverride (name = "id", column = @Column(name="id")),
		@AttributeOverride (name = "name", column = @Column(name="name")),
		@AttributeOverride (name = "genres", column = @Column(name="genres")),
		@AttributeOverride (name = "description", column = @Column(name="description")),
		@AttributeOverride (name = "url", column = @Column(name="url")),
		})
public class Movie extends Show {
	public Movie(Integer id, String name, String description) {
		super(id, name, description);
	}
	public Movie() {
	}
}
