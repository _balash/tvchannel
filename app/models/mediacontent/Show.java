package models.mediacontent;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "shows")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Show {

    @Id
    @GeneratedValue
    private Integer id;

    @Column
    private String name;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "genres")
    @Enumerated(EnumType.ORDINAL)
    private List<Genre> genres;

    @Column
    private String description;

    @Column
    private String url;

    public Show() {
        this.genres = new ArrayList<Genre>();
    }

    public Show(Integer id, String name, String description) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.genres = new ArrayList<Genre>();
    }

    @Override
    public String toString() {
        return "***Show***" + System.lineSeparator()
                + name + System.lineSeparator()
                + description + System.lineSeparator();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Genre[] getGenres() {
        return (Genre[]) genres.toArray();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
