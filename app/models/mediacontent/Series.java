package models.mediacontent;

import javax.persistence.*;

@Entity
@Table(name="series")
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name="id")),
		@AttributeOverride (name = "name", column = @Column(name="name")),
		@AttributeOverride (name = "genres", column = @Column(name="genres")),
		@AttributeOverride (name = "description", column = @Column(name="description")),
		@AttributeOverride (name = "url", column = @Column(name="url")),
		@AttributeOverride (name = "episodes", column = @Column(name="episodes"))
})
public class Series extends EpisodicShow {
	
	public Series() {
		super();
	}
	
	public Series(Integer id, String name, String description) {
		super(id, name, description);
	}
}
