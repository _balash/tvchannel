package nure.app.test;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import nure.app.models.Article;
import nure.app.models.Presenter;
import nure.app.models.Schedule;
import nure.app.models.ScheduleItem;
import nure.app.models.account.Role;
import nure.app.models.account.User;
import nure.app.models.mediacontent.AuthorShow;
import nure.app.models.mediacontent.Episode;
import nure.app.models.mediacontent.Movie;
import nure.app.models.mediacontent.Series;
import nure.app.models.mediacontent.Show;

public class Demo {
	static List<Article> articles = new ArrayList<Article>();
	static List<Presenter> presenters = new ArrayList<Presenter>();
	static List<ScheduleItem> items = new ArrayList<ScheduleItem>();
	static List<User> users = new ArrayList<User>();
	static List<Show> shows = new ArrayList<Show>();
	static List<Episode> episodes = new ArrayList<Episode>();

	public static void main(String[] args) {
		init();
		System.out.println("##########ARTICLES##########");

		for (Article article : articles) {
			System.out.println(article);
		}
		
		System.out.println("##########USERS##########");

		for (User user : users) {
			System.out.println(user);
		}
		
		System.out.println("##########PRESENTERS##########");

		for (Presenter presenter : presenters) {
			System.out.println(presenter);
		}
		
//		System.out.println("##########EPISODES##########");
//
//		for (Episode episode : episodes) {
//			System.out.println(episode);
//		}
	}

	private static void init() {
		articles.add(new Article(1, "Что там у хохлов?", "все нормальненько"));
		articles.add(
				new Article(2, "Новый альбом Жанны Фриске",
						"1.Ракировка\n" + "2.Я уеду жить в Ирак\n" + "3.Кто на горе свистнул\n"
								+ "4.Press 'ok' or 'cancer'\n" + "5.Ктоооо проживает на дне океана?\n"
								+ "6.Мои тиммейты"));
		articles.add(new Article(3, "ШОК! #aldec",
				"Константин Маслюк, в силу своей мудрости, лишился работы (зато не руки)"));
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		presenters.add(new Presenter(1, "Максим Доши", new LocalDate(1985,7,22),
				"Что-то на уровне Высоцкого, а может даже и выше (с)мамка"));
		presenters.add(new Presenter(2, "Саша Грей", new LocalDate(1988,11,2),
				"Что-то на уровне Высоцкого, а может даже и глубже (с)батя"));
		presenters.add(new Presenter(3, "Гуф", new LocalDate(1977,1,21),
				"Что-то на уровне Высоцкого, а может даже и мертвее (с)школьник"));
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		users.add(new User(1, "xxxAndryh@xxx", "pass", "Андрей", "motherfucker@mail.ru", Role.USER));
		users.add(new User(2, "admin", ")|(0p@", "Георгий", "georgiy.smeshnayafamilia@nure.ua", Role.ADMIN));
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		shows.add(new Movie(1, "Борат", "Телеведущий из Казахстана Борат отправляется в США, "
				+ "чтобы сделать репортаж об этой «величайшей в мире стране». "
				+ "Однако по прибытии оказалось, что главная цель его визита — "
				+ "поиски Памелы Андерсон с целью жениться на ней, а вовсе не " + "съемки документального фильма…"));
		shows.add(new Series(2, "Физрук",
				"Главный герой Фома всю жизнь был «правой рукой» влиятельного "
						+ "человека с полукриминальным прошлым. Когда «хозяин» "
						+ "выгнал его на пенсию, Фома решил любым способом вернуться обратно."));
		shows.add(new AuthorShow(3, "Званый ужин", "Кто из нас не мечтал пригласить своих гостей на званый ужин "
				+ "и создать незабываемую атмосферу праздника, уюта и " + "непринужденного общения?! "));
		shows.add(new AuthorShow(4, "Дурнев +1",
				"Не готовы к тонкому юмору и жесткому стебу? Оглядывайтесь по сторонам и удирайте!"));
		//////////////////////////////////////////////////////////////////////////////////////////////////////

		Episode fizruk1 = new Episode(1, "Подстава", "физрука подставили");
		Episode fizruk2 = new Episode(2, "Разочаровние", "усачу не светит");
		Episode dinner1 = new Episode(3, "Рэп от коляна",
				"А Алена, Юлькина подруга\n\t" + "Вызывала у мужчин лишь чувство испуга.\n\t"
						+ "Тощая, вертлявая, злая, рябая\n\t" + "Соблазнила-бы только Хана Мамая.\n\t"
						+ "Юлька рядом с ней-сексуальная красавица\n\t" + "Любому олигарху и без пол-литра понравится\n\t"
						+ "Жопа целлюлитная, но вполне аппетитная.\n\t" + "Грудь хоть и отвисла, но смотрится не кисло.");
		Episode dinner2 = new Episode(4, "Гостепреимная Александра", "Саша развлекает троих гостей одновременно");
		Episode durnev1 = new Episode(5, "Быдло в Балаклее", "Сотрудник Aldec показывает плохие результаты");
		Episode durnev2 = new Episode(6, "Быдло в Краматорске", "Еще один сотрудник Aldec потерпел неудачу");
		episodes.add(fizruk1);
		episodes.add(fizruk2);
		episodes.add(dinner1);
		episodes.add(dinner2);
		episodes.add(durnev1);
		episodes.add(durnev2);

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		Movie borat = (Movie) shows.get(0);
		AuthorShow dinner = (AuthorShow) shows.get(2);
		dinner.addEpisode(dinner1);
		dinner.addEpisode(dinner2);

		dinner.addPresenter(presenters.get(1));
		dinner.addPresenter(presenters.get(2));

		AuthorShow durnev = (AuthorShow) shows.get(3);
		durnev.addEpisode(durnev1);
		durnev.addEpisode(durnev2);

		durnev.addPresenter(presenters.get(1));
		durnev.addPresenter(presenters.get(0));

		Series fizruk = (Series) shows.get(1);
		fizruk.addEpisode(fizruk1);
		fizruk.addEpisode(fizruk2);
		
		System.out.println("##########SHOWS##########");

		for (Show show : shows) {
			System.out.println(show);
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////

		items.add(new ScheduleItem(2, new LocalDate(2016, 4, 26), new LocalTime(13, 00, 00), new LocalTime(14, 50, 00),
				borat));
		items.add(new ScheduleItem(1, new LocalDate(2016, 4, 26), new LocalTime(19, 20, 00), new LocalTime(19, 50, 00),
				fizruk));
		items.add(new ScheduleItem(3, new LocalDate(2016, 4, 27), new LocalTime(19, 20, 00), new LocalTime(19, 50, 00),
				fizruk));
		items.add(new ScheduleItem(4, new LocalDate(2016, 4, 27), new LocalTime(19, 50, 00), new LocalTime(21, 30, 00),
				dinner));
		items.add(new ScheduleItem(5, new LocalDate(2016, 4, 28), new LocalTime(11, 20, 00), new LocalTime(12, 10, 00),
				durnev));

		Schedule schedule = new Schedule();
		for (ScheduleItem item : items) {
			schedule.addItem(item);
		}
		System.out.println(schedule);
	}
}
